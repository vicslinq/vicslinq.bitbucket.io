<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Arden_and_Newton
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">


	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'ardennewton' ); ?></a>
	<header class="masthead visually-hidden" role="banner" data-loadmodule="Header">
        <div class="masthead-left">
            <a class="logo-header-link" href="../index.html">
                <h1 style="padding-left: 75%;" class="logo">
                    <span class="logo-text">Arden and Newton</span>
                    <img src="assets/images/logoo.png">

                </h1>
            </a>

        </div>

        <div class="masthead-right">
            <button type="button" class="navTrigger" aria-controls="site-navigation" aria-expanded="false">
                <span class="navTrigger-outer">
                    <span class="navTrigger-inner"></span>
                </span>
                <span class="navTrigger-instructions">Toggle Navigation</span>
            </button>
        </div>
    </header>
	<div class="masthead-scrollbar-mask"></div>
    <div class="site-navigation" id="site-navigation" aria-hidden="true">
       <?php
	       wp_nav_menu(array {
			'theme_location' => 'primary',
			'container' => 'nav',
 			'container_class' => 'site-navigation--nav',
			'menu_class' => 'site-navigation--primary-links'

		   })
	   ?>
	   
	   
	   
	    <nav class="site-navigation--nav" role="navigation">
            <ul class="site-navigation--primary-links">
                <li class="site-navigation--link-container">
                    <a aria-label="Company" class="link--underline" href="pages/aboutus.html">About Us</a>
                </li>
                <li class="site-navigation--link-container">
                    <a aria-label="Work" class="link--underline" href="pages/work.html">Our Work</a>
                </li>
                <li class="site-navigation--link-container">
                    <a aria-label="Insights" class="link--underline" href="pages/blog.html">Blog</a>
                </li>

            </ul>

            <ul class="site-navigation--secondary-links">

                <li class="site-navigation--link-container">
                    <a class="link--underline" aria-label="Careers" href="pages/career.html">Careers</a>
                </li>
                <li class="site-navigation--link-container">
                    <a class="link--underline" aria-label="Contact" href="pages/contact.html">Contact</a>
                </li>
            </ul>
        </nav>
    </div>
	

	<div id="content" class="site-content">
